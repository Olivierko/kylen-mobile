//The extensions for javascript prototypes
(function(){
	
	String.prototype.format = function() {
	    var formatted = this;
	    for (var i = 0; i < arguments.length; i++) {
	        var regexp = new RegExp('\\{' + i + '\\}', 'gi');
	        formatted = formatted.replace(regexp, arguments[i]);
	    }
	    return formatted;
	};
	
	Array.prototype.contains = function(obj) {
	    var i = this.length;
	    while (i--) {
	        if (this[i] === obj) {
	            return true;
	        }
	    }
	    return false;
	};
	
	Date.prototype.toPrettyString = function(){
		
		var year = this.getFullYear();
		var month = this.getMonth() + 1;
		var day = this.getDate();
		var hours = this.getHours();
		var minutes = this.getMinutes() > 9 ? this.getMinutes() : '0' + this.getMinutes();
		
		return '{0}:{1} {2}/{3}/{4}'.format(hours, minutes, day, month, year);
	};
	
	Date.prototype.addDays = function(days){
		var currentDate = new Date();
		
		var hoursToAdd = (days* 24);
		
		currentDate.setTime(currentDate.getTime() + (1000*3600*hoursToAdd));
		
		return currentDate;
	};
})();
