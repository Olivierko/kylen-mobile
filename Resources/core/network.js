(function(){
	kylen.network = {
		getJson: function(url, successCallback, errorCallback){
			if (typeof(successCallback) === 'undefined' || typeof(errorCallback) === 'undefined') {
				throw {
					message: 'Callbacks must be defined.'
				};
			};
			
			var onError = function(error){
				
				//Log?
				errorCallback(error);
			};
			
			var onSuccess = function(response){
				
				var jsonResult = JSON.parse(response);
				
				successCallback(jsonResult);
			};
			
			var xhr = kylen.network.createXhr(onSuccess, onError); 
			
			xhr.open('GET', url);
			
			xhr.send();
		},
		postJson: function(url, data, successCallback, errorCallback){
			
			var onError = function(error){

				//Log?
				errorCallback(error);
			};
			
			var onSuccess = function(response){
				
				var jsonResult = JSON.parse(response);
				
				successCallback(jsonResult);
			};
			
			var xhr = kylen.network.createXhr(onSuccess, onError); 
		
			xhr.open('POST', url);
			xhr.setRequestHeader('enctype', 'multipart/form-data');
			xhr.setRequestHeader('Content-Type', 'application/json; charset=utf-8');
			
			var jsonData = JSON.stringify(data);
			
			xhr.send(jsonData);
		},
		createXhr: function(successCallback, errorCallback){
			
			var xhr = Titanium.Network.createHTTPClient();
			
			xhr.onload = function(){
				successCallback(this.responseText);
			};
			
			xhr.onerror = function(error){
				errorCallback(error);
			};
			
			return xhr;
		}
	};
})();
