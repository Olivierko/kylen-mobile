var kylen = {};

(function(){
	
	//Colors	
	kylen.mainColor = '#525252';
	kylen.secondColor = '#919191';
	kylen.firstRowColor = '#757574';
	kylen.secondRowColor = '#BABABA';
	kylen.hoverStateColor = '#E6B140';
	kylen.productBorderColor = '#E6B140';
	kylen.productBoxBackgroundColor = '#FFFFFF';
	kylen.minusColor = '#F00000';
	kylen.plusColor = '#24F000';
	kylen.dateColorEven = '#EBB32F';
	kylen.dateColorOdd = '#BF8F1D';
	
	//Keys
	kylen.serviceRootUrl = 'http://192.168.120.102:3000';
	
	//Imports
	Ti.include('extensions.js');
	Ti.include('network.js');
	Ti.include('../ui/common/login/loginWindow.js');
	Ti.include('../ui/common/appTab/appTab.js');
	Ti.include('../ui/common/appTab/shop.js');
	Ti.include('../ui/common/appTab/purchases.js');
	Ti.include('../ui/common/appTab/stats.js');
})();
