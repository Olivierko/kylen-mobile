(function(){
	kylen.appTab.stats = {
		load: function(user){
			
			kylen.appTab.stats.user = user;
			
			var statsWindow = Ti.UI.createWindow({
				backgroundColor: kylen.mainColor
			});
			
			var webView = Ti.UI.createWebView({
				url:'/../../../html/chart.html',
				width:'100%',
				height:'100%'
			});
			statsWindow.add(webView);
			
			statsWindow.addEventListener('focus', function(e){
								kylen.appTab.stats.getStatsByToday();
			});			
						
			return statsWindow;
		},
		getStatsByToday: function(){
			
			var activityIndicator = Ti.UI.createActivityIndicator({
				height: 50,
				width: 10,
				message: 'Läser in statistik ...'
			});
			activityIndicator.show();
			
			var url = '{0}/json/productsStatsByDate'.format(kylen.serviceRootUrl);
			
			var today = new Date();
			
			today.setHours(0);
			today.setMinutes(0);
			
			var tomorrow = today.addDays(1);
			
			var data = {
				startDate: today.toString(),
				endDate: tomorrow.toString()
			};
			
			var onProductsStatsByDateError = function(error){
				alert(error);
			};
			
			var onProductsStatsByDateSuccess = function(results){
				
				var title = 'Produkter köpta idag';
				var products = [];
				var quantities = [];
				
				//Defaults for charts is to have a empty first!
				products.push('');
				quantities.push('');
				
				for (var i=0; i < results.length; i++) {
			  		var aggregatedResult = results[i];
			  		
			  		products.push(aggregatedResult.product.name);
			  		quantities.push(aggregatedResult.quantity);
				};
				
				var chartData = {
					title: title,
					titles: products,
					values: quantities
				};
				
			 	Ti.App.fireEvent('productsByTodayReady', {
            		chartData: chartData
        		});
        		
        		activityIndicator.hide();
			};
			
			kylen.network.postJson(url, data, onProductsStatsByDateSuccess, onProductsStatsByDateError);
		}
	};
})();
