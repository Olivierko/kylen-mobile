(function(){
	kylen.appTab = {
		load: function(user){
			
			kylen.appTab.tabGroup = Ti.UI.createTabGroup();
			
			kylen.appTab.shopTab = Ti.UI.createTab({
				title: 'Shop',
				icon: '/images/shop.png',
				window: kylen.appTab.shop.load(user)
			});	
			kylen.appTab.tabGroup.addTab(kylen.appTab.shopTab);
			
			kylen.appTab.purchaseTab = Ti.UI.createTab({
				title: 'Historik',
				icon: '/images/history.png',
				window: kylen.appTab.purchases.load(user)
			});	
			kylen.appTab.tabGroup.addTab(kylen.appTab.purchaseTab);
			
			kylen.appTab.statsTab = Ti.UI.createTab({
				title: 'Statistik',
				icon: '/images/chart.png',
				window: kylen.appTab.stats.load(user)
			});	
			kylen.appTab.tabGroup.addTab(kylen.appTab.statsTab);
			
			return kylen.appTab.tabGroup; 
		}
	};
})();
