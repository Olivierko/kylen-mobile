(function(){
	
	kylen.appTab.purchases = {
		load: function(user){
			
			kylen.appTab.purchases.user = user;
			
			var purchasesWindow = Ti.UI.createWindow({
				backgroundColor: kylen.mainColor
			});
			
			purchasesWindow.addEventListener('focus', function(){
				//Each time the window loads, populate the table with new data.
				kylen.appTab.purchases.populateTable();
			});
			
			kylen.appTab.purchases.purchasesList = Ti.UI.createTableView({
				backgroundColor: kylen.secondColor,
				width: '100%',
				height: '100%'
			});
			
			purchasesWindow.add(kylen.appTab.purchases.purchasesList);
			
			return purchasesWindow;
		},
		populateTable: function(){
			var onGetUserPurchasesSuccess = function(result){
				var purchaseRows = [];
			
				//create the header for a balance summary with a aggregated total spend amount and the balance itself.
				var balance = result.user.balance;
				
				var totalSpent = 0;
				
				for (var i=0; i < result.purchases.length; i++) {
			  		var purchase = result.purchases[i];
			  		
			  		totalSpent += (purchase.quantity * purchase.product.price);
				};
				
				var rowHeader = kylen.appTab.purchases.createHeader(balance, totalSpent);
				
				purchaseRows.push(rowHeader);
			
				for (var i=0; i < result.purchases.length; i++) {
				  	var purchase = result.purchases[i];
				  	
					var rowItem = kylen.appTab.purchases.createRow(purchase, i);
				  	
				  	purchaseRows.push(rowItem);
				};
				
				kylen.appTab.purchases.purchasesList.setData(purchaseRows);
				
				activityIndicator.hide();
			};
			
			var onGetUserPurchasesError = function(error){
				activityIndicator.hide();
				
				alert(error);
			};
			
			var activityIndicator = Ti.UI.createActivityIndicator({
				height: 50,
				width: 10,
				message: 'Läser in historik ...'
			});
			activityIndicator.show();
			
			kylen.appTab.purchases.getUserPurchases(onGetUserPurchasesSuccess, onGetUserPurchasesError);
		},
		getUserPurchases: function(successCallback, errorCallback){
			
			var url = '{0}/json/getUserPurchases/{1}'.format(kylen.serviceRootUrl, kylen.appTab.purchases.user._id);
			
			kylen.network.getJson(url, successCallback, errorCallback);
		},
		createHeader: function(balance, totalSpent){
			
			var rowHeader = Ti.UI.createTableViewRow({
		  		height: 100,
		  		backgroundColor: kylen.secondColor
		  	});
			
			var header = Ti.UI.createView({
				height: 100,
				width: '100%'
			});
			
			var balanceTitle = Ti.UI.createLabel({
				top: 10,
				text: 'Ditt nuvarande saldo: {0}:- kr'.format(balance),
				color: '#FFFFFF',
				font: {fontSize: 42, fontWeight: 'bold'}
			});
			header.add(balanceTitle);
			
			var totalSpentTitle = Ti.UI.createLabel({
				top: 60,
				text: '(totalt handlat för: {0}:- kr)'.format(totalSpent),
				color: '#FFFFFF',
				font: {fontSize: 24}
			});
			header.add(totalSpentTitle);
			
			rowHeader.add(header);
			
			return rowHeader;
		},
		createRow: function(purchase, index){
			
			var backgroundColor = (index % 2 == 0) ? kylen.firstRowColor : kylen.secondRowColor;
			var dateColor = (index % 2 == 0) ? kylen.dateColorEven : kylen.dateColorOdd;
			
			var rowItem = Ti.UI.createTableViewRow({
		  		height: 150,
		  		backgroundColor: backgroundColor
		  	});
		  	
		  	var purchaseView = Ti.UI.createView({
				height: 150,
				width: '90%',
				left: 20
			});
			
			var productImage = Ti.UI.createImageView({
				height: 75,
				width: 75,
				left: 10,
				image: purchase.product.imageUrl
			});
			purchaseView.add(productImage);
			
			var date = new Date(purchase.date);
			
			var productText = 
				' ' + 
				purchase.quantity + 
				' x ' + 
				purchase.product.name + 
				' (' + 
				(purchase.quantity * purchase.product.price) + 
				':- kr)';  	
			  	
		  	var productLabel = Ti.UI.createLabel({
		  		text: productText, 
		  		top: 20,
		  		left: 95,
		  		color: '#FFFFFF',
		  		font: {fontSize: 34}
		  	});
		  	purchaseView.add(productLabel);
		  	
		  	var purchaseLabel = Ti.UI.createLabel({
		  		text: date.toPrettyString(), 
		  		bottom: 35,
		  		left: 105,
		  		color: dateColor,
		  		font: {fontSize: 26, fontWeight: 'bold'}
		  	});
		  	purchaseView.add(purchaseLabel);
		  	
		  	rowItem.add(purchaseView);
		  	
		  	return rowItem;
		}
	};
})();
