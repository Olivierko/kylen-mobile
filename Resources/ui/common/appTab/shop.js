(function(){
	kylen.appTab.shop = {
		load: function(user){
			
			kylen.appTab.shop.user = user;
			
			var shopWindow = Ti.UI.createWindow({
				backgroundColor: kylen.mainColor
			});
			
			shopWindow.addEventListener('focus', function(){
				//Each time the window loads, populate the table with new data.
				kylen.appTab.shop.populateTable();
			});
			
			kylen.appTab.shop.shopList = Ti.UI.createTableView({
				backgroundColor: kylen.secondColor,
				width: '100%',
				height: '100%'
			});
			
			shopWindow.add(kylen.appTab.shop.shopList);
			
			return shopWindow;
		},
		populateTable: function(){
			var onGetProductsSuccess = function(products){
				var productRows = [];
			
				for (var i=0; i < products.length; i++) {
				  	var product = products[i];
					var rowItem = kylen.appTab.shop.createRow(product, i);
				  	
				  	productRows.push(rowItem);
				};
				
				kylen.appTab.shop.shopList.setData(productRows);
				
				activityIndicator.hide();
			};
			
			var onGetProductsError = function(error){
				activityIndicator.hide();
				
				alert(error);
			};
			
			var activityIndicator = Ti.UI.createActivityIndicator({
				height: 50,
				width: 10,
				message: 'Läser in drickor ...'
			});
			activityIndicator.show();
			
			kylen.appTab.shop.getProducts(onGetProductsSuccess, onGetProductsError);
		},
		getProducts: function(successCallback, errorCallback){
			
			var url = '{0}/json/getProducts'.format(kylen.serviceRootUrl);
			
			kylen.network.getJson(url, successCallback, errorCallback);
		},
		createRow: function(product, index){
			
			var backgroundColor = (index % 2 == 0) ? kylen.firstRowColor : kylen.secondRowColor;
			
			var rowItem = Ti.UI.createTableViewRow({
		  		height: 150,
		  		backgroundColor: backgroundColor,
		  		product: product,
		  		backgroundSelectedColor: '#E6B140',
		  		backgroundFocusedColor: '#E6B140'
		  	});
		  	
		  	rowItem.addEventListener('click', function(e){
		  		var product = e.row.product;
		  	
		  		kylen.appTab.shop.openDetailedView(product);
		  	});
			  	
			var productView = Ti.UI.createView({
				height: 150,
				width: '90%',
				left: 20
			});
			
			var productImage = Ti.UI.createImageView({
				height: 75,
				width: 75,
				left: 10,
				image: product.imageUrl
			});
			productView.add(productImage);
			  	
		  	var productName = Ti.UI.createLabel({
		  		top: 30,
		  		text: product.name + ' ' + product.price + ':-',
		  		left: 95,
		  		color: '#FFFFFF',
		  		font: {fontSize: 38}
		  	});
		  	productView.add(productName);
		  	
		  	var productStatus = Ti.UI.createLabel({
		  		top: 100,
		  		text: '(antal i lager: ' + product.quantity + ' st)',
		  		left: 100,
		  		color: '#FFFFFF',
		  		font: {fontSize: 26}
		  	});
		  	productView.add(productStatus);
		  	
		  	var arrow = Ti.UI.createImageView({
		  		image: '/images/arrow.png',
		  		right: 10
		  	});
		  	productView.add(arrow);

			rowItem.add(productView);
			
			return rowItem;
		},
		purchase: function(productId, quantity){
			
			var url = '{0}/json/buy'.format(kylen.serviceRootUrl);
			
			var data = {
				userId : kylen.appTab.shop.user._id,
				productId: productId,
				quantity: quantity
			}
			
			var onPurchaseError = function(error){
				activityIndicator.hide();
				
				alert('Något gick sönder!');
			};
			
			var onPurchaseSuccess = function(status){
				
				activityIndicator.hide();
				
				if (status.success) {
					//OK.
					kylen.appTab.shop.detailedProductWindow.close();
				}else{
					alert('Något gick sönder!');
				}
			};
			
			var activityIndicator = Ti.UI.createActivityIndicator({
				height: 50,
				width: 10,
				message: 'Genomför köp ...'
			});
			activityIndicator.show();
			
			kylen.network.postJson(url, data, onPurchaseSuccess, onPurchaseError);
		},
		openDetailedView: function(product){
			
			kylen.appTab.shop.detailedProductWindow = Ti.UI.createWindow({
				backgroundColor: kylen.mainColor
			});
			
			var productView = Ti.UI.createView({});
			
			var backButton = Ti.UI.createImageView({
				top: 5,
				left: 20,
				width: 75,
				height: 75,
				image: '/images/back_button.png'
			});
			productView.add(backButton);
			
			backButton.addEventListener('click', function(){
				kylen.appTab.shop.detailedProductWindow.close();
			});
			
			var productImageContainer = Ti.UI.createView({
				top: 100,
				left: 20,
				height: '35%',
				width: '40%',
				backgroundColor: kylen.productBoxBackgroundColor,
				borderColor: kylen.productBorderColor,
				borderRadius: 20,
				borderWidth: 4
			});
			
			var productImage = Ti.UI.createImageView({
				image: product.imageUrl,
				height: '80%'
			});
			productImageContainer.add(productImage);
			
			var productInformationContainer = Ti.UI.createView({
				top: 100,
				right: 20,
				height: '35%',
				width: '50%'
			});
			
			var productPrice = Ti.UI.createLabel({
				top: 0,
				text: product.price + ':- kr',
				color: '#FFFFFF',
				font: {fontSize: 50, fontWeight: 'bold'}
			});
			productInformationContainer.add(productPrice);
			
			var productPriceSeperator = Ti.UI.createView({
				height: '2px',
				width: '100%',
				backgroundColor: '#FFFFFF',
				top: 70
			});
			productInformationContainer.add(productPriceSeperator);
			
			var productName = Ti.UI.createLabel({
				top: 75,
				text: product.name,
				color: '#FFFFFF',
				font: {fontSize: 34}
			});
			productInformationContainer.add(productName);
			
			var productStatusContainer = Ti.UI.createView({
				width: '90%',
				height: '10%',
				top: '45%'
			});
			
			var productStatusLabel = Ti.UI.createLabel({
				top: 0,
				left: 0,
				font: {fontSize: 36, fontWeight: 'bold'},
				color: '#FFFFFF',
				text: 'Lager status: ' +  product.quantity + ' på lager'
			});
			productStatusContainer.add(productStatusLabel);
			
			var productStatusSeperator = Ti.UI.createView({
				height: '2px',
				width: '100%',
				backgroundColor: '#FFFFFF',
				top: 55
			});
			productStatusContainer.add(productStatusSeperator);
			
			var productInteractionContainer = Ti.UI.createView({
				top: '55%',
				width: '90%',
				height: '40%'
			});
			
			var decrementButton = Ti.UI.createView({
				top: 0,
				left: 30,
				height: 240,
				width: 240,
				backgroundImage: '/images/button_subt.png'
			});
			productInteractionContainer.add(decrementButton);
			
			decrementButton.addEventListener('click', function(){
				var currentAmount = quantityText.quantity;
				
				if (currentAmount <= 0) {
					return;
				};
				
				var currentTotalPrice = priceText.totalPrice - product.price;
				currentAmount--;
				
				//Set the new amounts by calling server etc then set the amounts below.
				
				quantityText.setText(currentAmount);
				quantityText.quantity = currentAmount;
				
				priceText.setText('Totalt: ' + currentTotalPrice + ':- kr');
				priceText.totalPrice = currentTotalPrice;
			});
			
			var incrementButton = Ti.UI.createView({
				top: 0,
				right: 30,
				height: 240,
				width: 240,
				backgroundImage: '/images/button_add.png'
			});
			productInteractionContainer.add(incrementButton);
			
			incrementButton.addEventListener('click', function(){
				var currentAmount = quantityText.quantity;
				
				var currentTotalPrice = priceText.totalPrice + product.price;
				currentAmount++;
				
				//Set the new amounts by calling server etc then set the amounts below.
				
				quantityText.setText(currentAmount);
				quantityText.quantity = currentAmount;
				
				priceText.setText('Totalt: ' + currentTotalPrice + ':- kr');
				priceText.totalPrice = currentTotalPrice;
			});
			
			var quantityText = Ti.UI.createLabel({
				top: 50,
				text: '0',
				quantity: 0,
				color: '#FFFFFF',
				font: {fontSize: 90, fontWeight: 'bold'}
			});
			productInteractionContainer.add(quantityText);
			
			var priceText = Ti.UI.createLabel({
				text: 'Totalt: 0:- kr',
				bottom: 130,
				totalPrice: 0,
				color: '#FFFFFF',
				font: {fontSize: 36, fontWeight: 'bold'}
			});
			productInteractionContainer.add(priceText);
			
			var purchaseButton = Ti.UI.createButton({
				title: 'Köp',
				bottom: 10,
				height: '25%',
				width: '90%'
			});
			productInteractionContainer.add(purchaseButton);
			
			purchaseButton.addEventListener('click', function(){
				var currentAmount = quantityText.quantity;
				
				if (currentAmount <= 0) {
					alert('Du dricker för lite.');
					
					return;
				};
				
				//Post!
				kylen.appTab.shop.purchase(product._id, currentAmount);
			});
			
			productView.add(productImageContainer);
			productView.add(productInformationContainer);
			productView.add(productStatusContainer);
			productView.add(productInteractionContainer);
			
			kylen.appTab.shop.detailedProductWindow.add(productView);
			
			kylen.appTab.shopTab.open(kylen.appTab.shop.detailedProductWindow);
		}
	};
})();
