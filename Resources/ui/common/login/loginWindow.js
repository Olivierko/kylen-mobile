(function(){
	kylen.login = {
		load: function(){
			
			kylen.login.loginWindow = Ti.UI.createWindow({
				backgroundColor: kylen.mainColor
			});	
			
			var logo = Ti.UI.createView({
				top: 0,
				width: 720,
				height: 138,
				backgroundColor: '#FFFFFF',
				backgroundImage: '/images/titlebar.png'
			});
			kylen.login.loginWindow.add(logo);
			
			var usersList = Ti.UI.createTableView({
				top: 138,
				backgroundColor: kylen.secondColor,
				width: '100%'
			});
			
			var onGetUsersSuccess = function(users){
				var userRows = [];
				
				for (var i=0; i < users.length; i++) {
				  	var user = users[i];
				  	
					var rowItem = kylen.login.createRow(user, i);
				  	
				  	userRows.push(rowItem);
				};
				
				usersList.setData(userRows);
				
				activityIndicator.hide();
			};
			
			var onGetUsersError = function(error){
				activityIndicator.hide();
				
				alert(error);
			};
			
			var activityIndicator = Ti.UI.createActivityIndicator({
				height: 50,
				width: 10,
				message: 'Läser in användare ...'
			});
			activityIndicator.show();
			
			kylen.login.getUsers(onGetUsersSuccess, onGetUsersError);
			
			kylen.login.loginWindow.add(usersList);
			
			return kylen.login.loginWindow;
		},
		getUsers: function(successCallback, errorCallback){
			
			var url = '{0}/json/getUsers'.format(kylen.serviceRootUrl);
			
			kylen.network.getJson(url, successCallback, errorCallback);
		},
		createRow: function(user, index){
			
			var backgroundColor = (index % 2 == 0) ? kylen.firstRowColor : kylen.secondRowColor;
			
			var rowItem = Ti.UI.createTableViewRow({
		  		height: 90,
		  		backgroundColor: backgroundColor,
		  		user: user,
		  		backgroundSelectedColor: kylen.hoverStateColor,
		  		backgroundFocusedColor: kylen.hoverStateColor
		  	});
		  	
		  	rowItem.addEventListener('click', function(e){
		  		var user = e.row.user;

		  		kylen.appTab.load(user).open();
		  	});
			  	
			var userView = Ti.UI.createView({
				height: 70,
				width: '90%',
				left: 20
			});
			  	
		  	var userName = Ti.UI.createLabel({
		  		text: user.name,
		  		top: 10,
		  		color: '#FFFFFF',
		  		font: {fontSize: 34}
		  	});
		  	userView.add(userName);
		  	
		  	var arrow = Ti.UI.createImageView({
		  		image: '/images/arrow.png',
		  		right: 10
		  	});
		  	userView.add(arrow);

			rowItem.add(userView);
			
			return rowItem;
		}		
	}; 	
})();
